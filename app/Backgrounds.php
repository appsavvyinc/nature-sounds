<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Backgrounds
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property int $activated 1-activated 0-not activated
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Sounds[] $sounds
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereActivated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Backgrounds whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Backgrounds extends Model
{

    const ACTIVATED = 1;
    /**
     * @var array
     */
    protected $guarded = [];

    public function sounds()
    {
        return $this->belongsToMany(Sounds::class, 'sounds_backgrounds');
    }
}
