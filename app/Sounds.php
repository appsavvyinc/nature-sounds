<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sounds
 *
 * @property int $id
 * @property string $title
 * @property string $file
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Backgrounds[] $backgrounds
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sounds whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sounds whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sounds whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sounds whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sounds whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sounds extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    
    public function backgrounds()
    {
        return $this->belongsToMany(Backgrounds::class, 'sounds_backgrounds');
    }
}
