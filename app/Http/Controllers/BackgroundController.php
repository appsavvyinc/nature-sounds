<?php

namespace App\Http\Controllers;

use App\Backgrounds;
use Illuminate\Http\Request;

class BackgroundController extends Controller
{
    public function getNatureSounds()
    {
        $backgrounds = Backgrounds::whereActivated(Backgrounds::ACTIVATED)->with('sounds')->get();

        $gifs = [];
        $i = 0;
        foreach ($backgrounds as $background){
            $gifs[$i]['backgroundGif'] = $background->image;
            $gifs[$i]['soundFile'] = $background->sounds[0]->file;
            $i++;
        }

        return \Response::json([
            'success'  => true,
            'data' => $gifs,
            'url' => env('CLOUDFRONT_BASE_LINK'),
            'soundsURI' => 'appsavvy/naturesounds/sounds/',
            'backgroundsURI' => 'appsavvy/naturesounds/backgrounds/'
        ]);
    }

    public function getSounds($soundName)
    {
        $soundName = strtolower($soundName);
        $soundUrl = "appsavvy/{$soundName}/sounds/";
        $backgroundUrl = "appsavvy/{$soundName}/backgrounds/";

        $soundFiles = \Storage::disk('s3')->allFiles($soundUrl);
        $backGroundFiles = \Storage::disk('s3')->allFiles($backgroundUrl);

        $gifs = [];
        $i = 0;
        foreach ($soundFiles as $soundFile) {
            $soundFile = str_replace($soundUrl, "", $soundFile);
            $fileName = pathinfo($soundFile, PATHINFO_FILENAME);

            if (!in_array($backgroundUrl.$fileName.".gif", $backGroundFiles)) {
                continue;
            }

            $gifs[$i]['backgroundGif'] = $fileName.".gif";
            $gifs[$i]['soundFile'] = $soundFile;
            $i++;
        }

        return \Response::json([
            'success'  => true,
            'data' => $gifs,
            'url' => env('CLOUDFRONT_BASE_LINK'),
            'soundsURI' => "appsavvy/{$soundName}/sounds/",
            'backgroundsURI' => "appsavvy/{$soundName}/backgrounds/"
        ]);
    }

}
