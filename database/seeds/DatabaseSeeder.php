<?php

use Illuminate\Database\Seeder;
use App\Backgrounds;
use App\Sounds;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Backgrounds::unguard();
        $background1 = Backgrounds::query()->create([
            'title' => 'Fire gif',
            'image' => 'fire_gif.gif'
        ]);
        $background2 =Backgrounds::query()->create([
            'title' => 'Natural lake waves gif',
            'image' => 'natural_lake_waves_gif.gif'
        ]);

        $background3 = Backgrounds::query()->create([
            'title' => 'Natural raining gif',
            'image' => 'natural_raining_gif.gif'
        ]);

        $background4 = Backgrounds::query()->create([
            'title' => 'Natural river gif',
            'image' => 'natural_river_gif.gif'
        ]);

        $background5 = Backgrounds::query()->create([
            'title' => 'Natural waterfall gif',
            'image' => 'natural_waterfall_gif.gif'
        ]);

        Sounds::unguard();
        Sounds::query()->create([
            'title' => 'Fire sound wav',
            'file' => 'fire_sound_wav.wav'
        ]);
        Sounds::query()->create([
            'title' => 'Natural lake waves wav',
            'file' => 'natural_lake_waves_wav.wav'
        ]);

        Sounds::query()->create([
            'title' => 'Natural raining wav',
            'file' => 'natural_raining_wav.wav'
        ]);

        Sounds::query()->create([
            'title' => 'Natural river wav',
            'file' => 'natural_river_wav.wav'
        ]);

        Sounds::query()->create([
            'title' => 'Natural waterfall wav',
            'file' => 'natural_waterfall_wav.wav'
        ]);

        $background1->sounds()->attach(1);
        $background2->sounds()->attach(2);
        $background3->sounds()->attach(3);
        $background4->sounds()->attach(4);
        $background5->sounds()->attach(5);
    }
}
